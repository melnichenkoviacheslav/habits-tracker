import logHello from './index';

it('calls console.log with "Hello, World!!!"', () => {
    const consoleSpy = jest.spyOn(console, 'log');

    logHello();

    expect(consoleSpy).toHaveBeenLastCalledWith('Hello, World!!!');
});
