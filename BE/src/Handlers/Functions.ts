import { Task } from '../Interfaces/Task';
import { Achievement } from '../Interfaces/Achievement';
import { Challenge } from '../Interfaces/Challenge';
import { Status } from '../Interfaces/Status';

// Returns a current task with its status by the challenge id
function getCurrentTask(Challenge: Challenge): Task | null {
    return null;
}

// Returns a list of actual achievements by the challenge id
function getAchievements(challenge: Challenge): Achievement[] {
    return [];
}

// Returns all past tasks with their results by the challenge id
function getTaskArchive(challenge: Challenge): Task[] {
    return [];
}

// Returns a new challenge using the following parameters: a list of tasks,
// a list of challenges, challenge duration that by default should be 30 days,
// number of achievements – by default, challenge duration
function startNewChallenge(
    tasks: Task[],
    challenges: Challenge[],
    challengeDuration = 30,
    achievementsQuantity = challengeDuration / 6,
): Challenge {
    return {} as Challenge;
}

// Returns achievements status for the challenge by its achievements list and tasks status
function calculateAchievementsStatus(
    achievements: Achievement[],
    tasks: Task[],
): Status[] {
    return [];
}
