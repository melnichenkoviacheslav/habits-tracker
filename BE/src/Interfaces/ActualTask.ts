import { Status } from './Status';

/**
ActualTask provides information about a task
and its current status in scope of the challenge
 */
export interface ActualTask {
    id: string;
    description: string;
    status: Status;
}
