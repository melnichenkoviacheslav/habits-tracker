/** Possible {@link Status} states */
export enum State {
    Pending = 'Pending',
    Success = 'Success',
    Failure = 'Failure',
}
