import { Status } from './Status';

/**
ArchiveItem describes a task and its status for all past tasks in the challenge
 */
export interface ArchiveItem {
    id: string;
    description: string;
    status: Status;
}
