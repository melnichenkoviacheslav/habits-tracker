import { Status } from './Status';

/**
ActualAchievement provides information about an achievement
and its current status in scope of the challenge
 */
export interface ActualAchievement {
    id: string;
    description: string;
    image: string;
    status: Status;
}
