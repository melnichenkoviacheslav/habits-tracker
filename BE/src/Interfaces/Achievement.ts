import { Status } from './Status';
import { Task } from './Task';

/**
Achievement describes a set of several tasks accomplished in the specific way.
For example: “Complete each task 7 days in a row” or “Complete 5 tasks before 8:00 AM”
 */
export interface Achievement {
    id: string;
    description: string;
    image: string;

    /**A method that can return an achievement status by tasks status*/
    checkComplete(task: Task[]): Status;
}
