/**
Task describes a single action that should be done by the user.
For example: “Go for a 10 minutes run” or “Go to bed before 11:00 PM”
 */
export interface Task {
    id: string;
    description: string;
}
