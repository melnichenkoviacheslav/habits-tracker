import { State } from './State';

/**
Status describes a state of some item (a task or an achievement)
and a timestamp, when this state was updated
*/
export interface Status {
    state: State;
    updated: Date;
}
