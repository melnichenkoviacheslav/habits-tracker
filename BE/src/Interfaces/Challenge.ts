import { Task } from './Task';
import { State } from './State';
import { Status } from './Status';

/**
Challenge describes a 30-days period, during which randomly chosen 30 tasks
and 5 achievements are assigned for the user. Starting from the first day,
the user will receive a new task every day, which should be completed before
the midnight, otherwise it will be marked as failed. Achievements status is calculated
based on tasks completion. After 30 days the challenge could be successful
(>= 90% tasks completed) or failed (<90% tasks completed)
 */
export interface Challenge {
    id: string;
    state: State;
    startDate: Date;
    tasksOrder: Task[];
    tasksStatus: Status[];
    achievementsStatus: Status[];
}
