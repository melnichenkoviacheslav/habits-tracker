import setBodyHello from './index';

it('body h1 equals "Hello, World!"', () => {
    setBodyHello();

    const actualText = document.querySelector('h1');
    expect(actualText?.textContent).toBe('Hello, World!');
});
